import { Module } from '@nestjs/common';
import { CarModule } from './car/car.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://admin_val:umZn9B7dG5RxjF9@nest-crash-rest-api-clu.fe7lz.mongodb.net/nest-car-db?retryWrites=true&w=majority&ssl=true',
      // {
      //   connectionName: 'nest-car-db-collection',
      // },
    ),
    CarModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
