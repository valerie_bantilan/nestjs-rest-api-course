import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Query,
} from '@nestjs/common';
import { CarService } from './car.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';
import { Car } from './interfaces/car.interface';

@Controller('cars')
export class CarController {
  constructor(private carService: CarService) {}

  @Get()
  async getCars(): Promise<Car[]> {
    return this.carService.getCars();
  }

  @Get(':id')
  async getCar(@Param() params): Promise<Car> {
    return this.carService.getCarById(params.id);
  }

  @Post()
  create(@Body() createItemDto: CreateCarDto): Promise<Car> {
    return this.carService.create(createItemDto);
  }

  @Delete(':id')
  delete(@Param('id') id): Promise<Car> {
    return this.carService.delete(id);
  }

  @Put(':id')
  update(@Body() updateItemDto: UpdateCarDto, @Param('id') id): Promise<Car> {
    return this.carService.update(id, updateItemDto);
  }
}
