import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CarController } from './car.controller';
import { CarService } from './car.service';
import { CarSchema } from './schemas/car.schema';

@Module({
  controllers: [CarController],
  providers: [CarService],
  imports: [MongooseModule.forFeature([{ name: 'Car', schema: CarSchema }])],
})
export class CarModule {}
