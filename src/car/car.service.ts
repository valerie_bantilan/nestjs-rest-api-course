import { Injectable, HttpException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Car } from './interfaces/car.interface';

@Injectable()
export class CarService {
  constructor(@InjectModel('Car') private readonly carModel: Model<Car>) {}

  async getCars(): Promise<Car[]> {
    return await this.carModel.find();
  }

  async getCarById(id: string): Promise<Car> {
    return await this.carModel.findOne({ _id: id });
  }

  async create(car: Car): Promise<Car> {
    const newCar = new this.carModel(car);
    return await newCar.save();
  }

  async delete(id: string): Promise<Car> {
    return await this.carModel.findByIdAndRemove(id);
  }

  async update(id: string, car: Car): Promise<Car> {
    return await this.carModel.findByIdAndUpdate(id, car, { new: true });
  }
}
