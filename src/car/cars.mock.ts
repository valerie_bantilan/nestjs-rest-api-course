export const CARS = [
  {
    id: 1,
    brand: 'Toyota',
    color: 'black',
  },
  {
    id: 2,
    brand: 'Chevrolet',
    color: 'white',
  },
  {
    id: 3,
    brand: 'BMW',
    color: 'gray',
  },
  {
    id: 4,
    brand: 'Honda',
    color: 'blue',
  },
];
