export class CreateCarDto {
  readonly brand: string;
  readonly color: string;
}
