export interface Car {
  id?: string | number;
  brand: string;
  color?: string;
}
